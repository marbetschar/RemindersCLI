# Reminders CLI

A CLI tool for interaction with the macOS Reminders app. The tool responds in JSON format and is therefore perfectly suitable for process automation or embedding.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Make sure you have Xcode and the Xcode Command Line Tools installed and you are ready to go. Please make sure, you got the correct Command Line Tools selected. For this execute the following command in a terminal:

```
$ xcode-select --print-path
/Applications/Xcode.app/Contents/Developer
```

If your output differs, execute the following:

```
$ sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
```

## Deployment

To build a production release execute the following command within the project root directory:

```
swift build -c release -Xswiftc -static-stdlib
cd .build/*macos*/release
```

Now you got a production ready binary at ´.build/release´. Copy this wherever you want for use. If you want to use the binary on your local machine, execute the following:

```
cp -f RemindersCLI /usr/local/bin/reminders
```

Now you can simply run `reminders` in your terminal. If you see the following output, you are ready to roll!

```
$ reminders

Usage: reminders <command> [options]

Command-line interface to interact with the Reminders.app

Groups:
  list            list tooling

Commands:
  ls              returns the reminders matching the given options. If no options are set, all reminders are returned.
  help            Prints help information
```

## Usage

If you just run the `reminders` command in terminal, it prints out the help section:

```
$ reminders

  Usage: reminders <command> [options]


  Commands:

    ls [options]				query reminders
    create <title> [options]	create reminder with <title>
    delete <id> [options]		delete reminder with <id>
    complete <id> [options]		complete reminder with <id>
    list [cmd]					list tooling
    help [cmd]					display help for [cmd]

  Options:

    -h, --help				output usage information
    -V, --version  			output the version number
    -f, --format <json>		output the result in the given format (e.g. json)
```

### Query reminders: `reminders ls`

```
$ reminders ls

  Usage: reminders ls [options]
  
  returns the reminders matching the given options. If no options are set, all reminders are returned.  

  Options:

    -h, --help       				output usage information
    -V, --version    				output the version number
    -f, --format <json>				output the result in the given format
	-l, --lists <id, ...> 			output reminders only of given lists (comma separated values)
	-c, --completed <true|false>	output completed or incompleted reminders (defaults to true)
	-f, --from <YYYY-MMM-DD>		output reminders with completion/due-date after given date
	-t, --to <YYYY-MM-DDD>			output reminders with completion/due-date before given date
```

### Interacting with lists: `reminders list ...`

```
$ reminders list

  Usage: reminders list <command> [options]
  
  
  Commands:

    ls [options]              	query lists
    create <title> [options]    create list with <title>
    delete <id> [options]       delete list with <id>
    help [cmd]                  display help for [cmd]
   
  Options:

    -h, --help       				output usage information
    -V, --version    				output the version number
    -f, --format <json>				output the result in the given format
```

### Query lists: `reminders list ls`

```
$ reminders list ls

  Usage: reminders list ls [options]

  returns all available lists.
   
  Options:

    -h, --help       				output usage information
    -V, --version    				output the version number
    -f, --format <json>				output the result in the given format
```

## Built With

* [SwiftCLI](https://github.com/jakeheis/SwiftCLI) - A powerful framework that can be used to develop a CLI in Swift
* [Swift Package Manager](https://swift.org/package-manager/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/marbetschar/RemindersCLI/tags).

## Authors

* **Marco Betschart** - *Initial work* - [@marbetschar](https://gitlab.com/marbetschar)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks for @jakeheis for providing [SwiftCLI](https://github.com/jakeheis/SwiftCLI) - it tremendously simplifies creating cli tools!