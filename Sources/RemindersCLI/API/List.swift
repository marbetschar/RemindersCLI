import EventKit

public struct List: Codable {
	public let id: String
	public let title: String?
	public let color: String?
	
	init(_ calendar: EKCalendar) {
		self.id = calendar.calendarIdentifier
		self.title = calendar.title
		self.color = calendar.color.hexString
	}
	
	init(id: String){
		self.id = id
		self.title = nil
		self.color = nil
	}
}
