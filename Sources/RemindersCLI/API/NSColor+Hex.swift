import Cocoa

extension NSColor {
	
	var hexString: String? {
		guard let rgb = self.usingColorSpace(.deviceRGB) else { return nil }
		let red = Int(round(rgb.redComponent * 0xFF))
		let green = Int(round(rgb.greenComponent * 0xFF))
		let blue = Int(round(rgb.blueComponent * 0xFF))
		
		return String(format: "#%02X%02X%02X", red, green, blue)
	}
}
