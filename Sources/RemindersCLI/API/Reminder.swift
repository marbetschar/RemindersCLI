import EventKit

public struct Reminder: Codable {
    public let id: String
    public let listId: String
    
    public let title: String
    public let notes: String?
    public let priority: Int
    public let completed: Bool

    public let location: String?
    public let url: URL?

    public let createdAt: Date?
    public let modifiedAt: Date?
    public let completedAt: Date?
    public let startAt: Date?
    public let dueAt: Date?

    init(_ reminder: EKReminder) {
        self.id = reminder.calendarItemIdentifier
        self.listId = reminder.calendar.calendarIdentifier
        
        self.title = reminder.title
        self.notes = reminder.notes
        self.priority = reminder.priority
        self.completed = reminder.isCompleted

        self.location = reminder.location
        self.url = reminder.url

        self.createdAt = reminder.creationDate
        self.modifiedAt = reminder.lastModifiedDate
        self.completedAt = reminder.completionDate
		
		self.startAt = reminder.startDateComponents?.date
		self.dueAt = reminder.dueDateComponents?.date
    }
}
