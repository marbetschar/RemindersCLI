import EventKit

import EventKit

public class Reminders {
	public static let shared = Reminders(store: EKEventStore())
	
	private let store: EKEventStore
	
	init(store: EKEventStore) {
    	self.store = store
	}

	
	// MARK: Reminders
	
	public func fetch(_ term: String? = nil, completedFrom from: Date? = nil, to: Date? = nil, in lists: [List]? = nil) throws -> [Reminder] {
		let calendars = try lists?.compactMap{ try calendar(withIdentifier: $0.id) }
		var to = to
		if from != nil, to == nil {
			to = Date()
		}
		return try reminders(term, matching: store.predicateForCompletedReminders(withCompletionDateStarting: from, ending: to, calendars: calendars))
    }
	
	public func fetch(_ term: String? = nil, dueFrom from: Date? = nil, to: Date? = nil, in lists: [List]? = nil) throws -> [Reminder] {
		let calendars = try lists?.compactMap{ try calendar(withIdentifier: $0.id) }
		var to = to
		if from != nil, to == nil {
			to = Date()
		}
		return try reminders(term, matching: store.predicateForIncompleteReminders(withDueDateStarting: from, ending: to, calendars: calendars))
	}

    public func fetch(_ term: String? = nil, in lists: [List]? = nil) throws -> [Reminder] {
		let calendars = try lists?.compactMap{ try calendar(withIdentifier: $0.id) }
        return try reminders(term, matching: store.predicateForReminders(in: calendars))
    }

	
	// MARK: Lists
	
    public func fetchLists() throws -> [List] {
        return try lists()
    }
	
	public func fetchList(withIdentifier identifier: String) throws -> List? {
		return try calendar(withIdentifier: identifier).map(List.init)
	}
	
	
	// MARK: Helpers
	
    private func lists() throws -> [List] {
        let semaphore = DispatchSemaphore(value: 0)
        var error: Error? = nil
        var lists = [List]()

        request(.reminder, errorHandler: {
            error = $0
            semaphore.signal()
            
        }) { store in
			lists = store.calendars(for: .reminder).map(List.init)
            semaphore.signal()
        }
        semaphore.wait()

        if let error = error {
            throw error
        }
        return lists
    }
	
	private func calendar(withIdentifier identifier: String) throws -> EKCalendar? {
		let semaphore = DispatchSemaphore(value: 0)
		var error: Error? = nil
		var calendar: EKCalendar?
		
		request(.reminder, errorHandler: {
			error = $0
			semaphore.signal()
			
		}) { store in
			calendar = store.calendar(withIdentifier: identifier)
			semaphore.signal()
		}
		semaphore.wait()
		
		if let error = error {
			throw error
		}
		return calendar
	}

    private func reminders(_ term: String? = nil, matching predicate: NSPredicate) throws -> [Reminder] {
        let semaphore = DispatchSemaphore(value: 0)
        var error: Error? = nil
        var reminders = [Reminder]()

        request(.reminder, errorHandler: {
            error = $0
            semaphore.signal()
            
        }) { store in
            store.fetchReminders(matching: predicate) {
				var allReminders = $0
				if let t = term?.lowercased(), false == t.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
					allReminders = allReminders?.filter{
						return ($0.title?.lowercased().contains(t) ?? false) || ($0.notes?.lowercased().contains(t) ?? false)
					}
				}
                reminders = allReminders?.map(Reminder.init) ?? []
                semaphore.signal()
            }
        }
        semaphore.wait()

        if let error = error {
            throw error
        }
        return reminders
    }
	

	private func request(_ entityType: EKEntityType, execute: @escaping ((EKEventStore) -> Void)) {
		self.request(entityType, errorHandler: { _ in }, execute: execute)
	}
	
	
	private func request(_ entityType: EKEntityType, errorHandler: @escaping ((Error?) -> Void), execute: @escaping ((EKEventStore) -> Void)) {
		let store = self.store
		
		store.requestAccess(to: entityType) { success, error in
			guard success else {
				errorHandler(error)
				return
			}
			execute(store)
		}
	}
}
