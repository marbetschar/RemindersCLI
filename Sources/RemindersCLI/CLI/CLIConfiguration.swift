import Foundation
import SwiftCLI

struct CLIConfiguration {
	static let formatGlobalOption = Key<String>("-f", "--format", description: "output the result in the given format")
	
	static let globalOptions: [Option] = [
		formatGlobalOption
	]
	
	static let isoDateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd"
		return formatter
	}()
	
	static let isoDateTimeFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
		return formatter
	}()
	
	static let jsonEncoder: JSONEncoder = {
		let encoder = JSONEncoder()
		encoder.dateEncodingStrategy = .formatted(isoDateTimeFormatter)
		encoder.keyEncodingStrategy = .convertToSnakeCase
		return encoder
	}()
}

extension Command {
	var isoDateFormatter: DateFormatter { return CLIConfiguration.isoDateFormatter }
	var jsonEncoder: JSONEncoder { return CLIConfiguration.jsonEncoder }
	
    var format: Key<String> { return CLIConfiguration.formatGlobalOption }
}
