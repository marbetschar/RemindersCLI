//
//  CLIError.swift
//  RemindersCLI
//
//  Created by Marco Betschart on 10.09.18.
//

import Foundation

struct CodableError: Codable {
	let status = "error"
	let message: String
	
	init(_ error: Error) {
		self.message = error.localizedDescription
	}
}
