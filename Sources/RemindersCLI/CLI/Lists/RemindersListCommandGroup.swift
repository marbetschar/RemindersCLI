import SwiftCLI

class RemindersListCommandGroup: CommandGroup {
	let name = "list"
	let shortDescription = "list tooling"
	
	let children: [Routable] = [
        RemindersListLs(),
		RemindersListGet()
    ]
}
