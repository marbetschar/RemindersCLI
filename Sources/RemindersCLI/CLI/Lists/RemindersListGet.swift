import Foundation
import SwiftCLI

class RemindersListGet: Command {
	let name = "get"
	let shortDescription = "returns a specific list by its id"
	
	let id = Parameter()
	
	func execute() throws {
		var list: List?
		var err: Error?
		
		do {
			list = try Reminders.shared.fetchList(withIdentifier: id.value)
		} catch {
			err = error
		}
		
		if let error = err {
			let json = try jsonEncoder.encode(CodableError(error))
			stderr <<< String(data: json, encoding: .utf8) ?? "{ \"status\": \"error\" }"
		} else {
			let json = try jsonEncoder.encode(list)
			stdout <<< "{ \"status\": \"success\", \"data\": " + (String(data: json, encoding: .utf8) ?? "{}") + " }"
		}
	}
}
