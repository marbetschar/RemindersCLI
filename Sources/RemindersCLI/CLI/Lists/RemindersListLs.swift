import Foundation
import SwiftCLI

class RemindersListLs: Command {
    let name = "ls"
    let shortDescription = "returns all lists"

    func execute() throws {
		var lists = [List]()
		var err: Error?
		
		do {
			lists = try Reminders.shared.fetchLists()
		} catch {
			err = error
		}
		
		if let error = err {
			let json = try jsonEncoder.encode(CodableError(error))
			stderr <<< String(data: json, encoding: .utf8) ?? "{ \"status\": \"error\" }"
		} else {
			let json = try jsonEncoder.encode(lists)
			stdout <<< "{ \"status\": \"success\", \"data\": " + (String(data: json, encoding: .utf8) ?? "[]") + " }"
		}
    }
}
