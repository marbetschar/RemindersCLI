import Foundation
import SwiftCLI

class RemindersLs: Command {
    let name = "ls"
    let shortDescription = "returns the reminders matching the given options. If no options are set, all reminders are returned."
	
	let term			= Key<String>("-t", "--term", description: "search for reminders containing the given text (the search is case-insensitive)")
    let lists       	= Key<String>("-l", "--lists", description: "reminders only of given lists (comma separated ids)")
	let completed		= Flag("-c", "--completed", description: "only completed reminders")
	let incomplete		= Flag("-i", "--incomplete", description: "only incomplete reminders")
	let completedFrom	= Key<String>("--completed-from", description: "reminders which are completed after given date")
	let completedTo		= Key<String>("--completed-to", description: "reminders which are completed before given date")
	let dueFrom			= Key<String>("--due-from", description: "reminders which are due after given date")
	let dueTo			= Key<String>("--due-to", description: "reminders which are due before given date")

    func execute() throws {
		var reminders = [Reminder]()
		var err: Error?
		
		do {
			let lists = self.lists.value?.split(separator: ",").map{ List(id: String($0)) }
			
			if completed.value == true || completedFrom.value != nil || completedTo.value != nil {
				var fromStartOfDay: Date? = nil
				if let from = isoDateFormatter.date(from: completedFrom.value ?? "") {
					fromStartOfDay = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: from)
				}
				
				var toEndOfDay: Date? = nil
				if let to = isoDateFormatter.date(from: completedTo.value ?? "") {
					toEndOfDay = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: to)
				}
				reminders = try Reminders.shared.fetch(term.value, completedFrom: fromStartOfDay, to: toEndOfDay, in: lists)
				
			} else if incomplete.value == true || dueFrom.value != nil || dueTo.value != nil {
				var fromStartOfDay: Date? = nil
				if let from = isoDateFormatter.date(from: dueFrom.value ?? "") {
					fromStartOfDay = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: from)
				}
				
				var toEndOfDay: Date? = nil
				if let to = isoDateFormatter.date(from: dueTo.value ?? "") {
					toEndOfDay = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: to)
				}
				reminders = try Reminders.shared.fetch(term.value, dueFrom: fromStartOfDay, to: toEndOfDay, in: lists)
				
			} else {
				reminders = try Reminders.shared.fetch(term.value, in: lists)
			}
		
		} catch {
			err = error
		}
		
		if let error = err {
			let json = try jsonEncoder.encode(CodableError(error))
			stderr <<< String(data: json, encoding: .utf8) ?? "{ \"status\": \"error\" }"
		} else {
			let json = try jsonEncoder.encode(reminders)
			stdout <<< "{ \"status\": \"success\", \"data\": " + (String(data: json, encoding: .utf8) ?? "[]") + " }"
		}
    }
}
