import SwiftCLI

let cli = CLI(
    name: "reminders",
    description: "Command-line interface to interact with the Reminders.app"
)

cli.globalOptions.append(contentsOf: CLIConfiguration.globalOptions)
cli.commands = [
    RemindersLs(),
    RemindersListCommandGroup()
]

cli.goAndExit()
