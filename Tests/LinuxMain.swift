import XCTest

import RemindersCLITests

var tests = [XCTestCaseEntry]()
tests += RemindersCLITests.allTests()
XCTMain(tests)